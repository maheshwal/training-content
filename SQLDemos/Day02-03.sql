--set operator

select employeeid,employeename,city from tblEmployee
union
select departmentid,departmentname,departmentloc from tbldepartment

select employeeid, employeename,city, salary from tblemployee
where salary between 20000 and 30000
union all
select employeeid, employeename,city, salary from tblemployee
where salary between 25000 and 35000

select employeeid, employeename,city, salary from tblemployee
where salary between 20000 and 30000
intersect
select employeeid, employeename,city, salary from tblemployee
where salary between 25000 and 35000

select employeeid, employeename,city, salary from tblemployee
where salary between 25000 and 35000
except
select employeeid, employeename,city, salary from tblemployee
where salary between 20000 and 30000

--joins
--get the employeenames and their departmentnames
select employeename,departmentname from
tblemployee join tbldepartment
on tblemployee.departmentid=tbldepartment.departmentid


select emp.employeeid,emp.employeename,dept.departmentid,dept.departmentname from
tblemployee emp left join tbldepartment dept
on emp.departmentid=dept.departmentid

select emp.employeeid,emp.employeename,dept.departmentid,dept.departmentname from
tblemployee emp right join tbldepartment dept
on emp.departmentid=dept.departmentid

select emp.employeeid,emp.employeename,dept.departmentid,dept.departmentname from
tblemployee emp full join tbldepartment dept
on emp.departmentid=dept.departmentid

--employeenames and their manager names
select emp2.employeename "Employee",emp1.employeename "Manager"
from tblemployee emp1 join tblemployee emp2
on emp2.managerid=emp1.employeeid

select * from tblemployee

select emp.employeeid,emp.employeename,dept.departmentid,dept.departmentname from
tblemployee emp cross join tbldepartment dept

--subquery
select employeename,salary from tblemployee 
where salary=
(select salary from tblemployee 
where employeename='Mary Jane') and employeename not like 'M%'
--get employeenames, salary of employess who work in the same department as John Smith

--get employees whose salary is greater than any salary value for department no.1
--any or all
select employeename,salary from tblemployee
where salary>any(select salary from tblemployee where departmentid=1)

select employeename,salary from tblemployee
where salary>all(select salary from tblemployee where departmentid=2)

--built-in functions

--varibale declaration
declare @someVar varchar(20)
set @someVar='Hello World!!!'
select @someVar
print @someVar

declare @someName varchar(20)
select @someName=employeename from tblEmployee where employeeid=1001
print @someName
set @someName='Good morning'
print @someName

--system functions
select HOST_ID(),HOST_NAME()
declare @hostname varchar(20),@hostid varchar(10)
select @hostid=HOST_ID(),@hostname=HOST_NAME()
print 'Host ID:'+@hostid+' Host Name:'+@hostname

--string functions


select ascii('c'),char(99)

declare @someString varchar(max)='This is a beautiful day' +' in a beautiful world'
+' filled with beautiful moments';
select len(@someString),CHARINDEX('beautiful',@someString,31)
select left(@someString,4),right(@someString,7)

select len('    Tendulkar    ')
select len(ltrim('    Tendulkar    ')),len(rtrim('    Tendulkar    '))

select replace('Donald Trump','Trump','Duck'),
replicate('HA...',10),reverse('Tendulkar')

select reverse(employeename) from tblEmployee

--Date and time functions
select getdate()
select day(getdate()),month(getdate())
select datepart(dd,getdate()),datepart(mm,getdate()),datepart(yy,getdate())
select DATENAME(mm, getdate())
select DATEADD(dd,2,getdate()),DATEADD(mm,2,getdate()),DATEADD(yy,2,getdate())
select DATEDIFF(yy,'2010-05-19',getdate()),DATEDIFF(mm,'2010-05-19',getdate()),
DATEDIFF(dd,'2010-05-19',getdate())

--conversion functions
select cast(getdate() as varchar)
select convert(varchar,getdate(),101)

declare @empid int
select @empid=employeeid from tblEmployee
where employeename='Jenna Smith'
print cast(@empid as varchar)

--aggregate functions(count,sum,max,min,avg)
select count(employeeid) from tblemployee
where DepartmentId=1
--group by
select DepartmentId,count(employeeid) "Count of Employees",
sum(salary) "Total salary",avg(salary) "Average Salary",min(salary) "Min Salary",
max(salary) "Max Salary" from tblEmployee
group by DepartmentId
having DepartmentId is null--DepartmentId<4--min(salary)>45000

select * from tblEmployee