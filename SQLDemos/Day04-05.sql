--error handling
create or alter proc usp_DemoError
as
begin try
	select 10/0
end try
begin catch
	--print 'Error occurred!!!'
	select ERROR_NUMBER() "Error No.",
	ERROR_MESSAGE() "Error",
	ERROR_LINE() "Line No.",
	ERROR_SEVERITY() "Error Criticality",
	ERROR_PROCEDURE() "Error Source",
	ERROR_STATE() "Error Location"
end catch


insert into tblPerson values(101,'Adam Smith','New York',10)
select * from tblPerson

alter table tblPerson
add constraint chk_age check(PAge>18)

create or alter proc usp_AddPerson(@ssn int,@name varchar(50),@city varchar(50),@age int)
as
begin try
	if(@age<18)
		raiserror(50006,16,1)
	else
		insert into tblPerson 
		values(@ssn,@name,@city,@age)
end try
begin catch
	select ERROR_NUMBER() "Error No.",
	ERROR_MESSAGE() "Error",
	ERROR_LINE() "Line No.",
	ERROR_SEVERITY() "Error Criticality",
	ERROR_PROCEDURE() "Error Source",
	ERROR_STATE() "Error Location"
end catch

exec usp_AddPerson 103,'Anna Smith',null,15

select * from sys.messages
where message_id=8134

exec sp_addmessage @msgnum=50006,@severity=11,@msgtext='Age should be greater than 18'

--perform the error handling in a stored procedure which inserts a record in your 
--employee table. Make sure to use raiserror and show the error msg from sys.messages

--user defined functions
--scalar
create or alter function udf_GetEmployeeName(@id int)
returns varchar(20) 
as
begin
	declare @name varchar(20)
	select @name=employeename from tblEmployee 
	where EmployeeId=@id
	return @name
end

select distinct dbo.udf_GetEmployeeName(1001) from tblEmployee
--table-valued
create or alter function udf_GetEmployeeInfo(@id int)
returns table 
as
return
	select * from tblEmployee 
	where EmployeeId=@id

select * from dbo.udf_GetEmployeeInfo(1001)

--transactions
--commit,rollback
--implicit->use commit,rollback explicity at the end of the statement 
--explicit->user decides the starting and ending point of a transaction

begin transaction transOps
update tblEmployee
set city='Los Angeles'
where EmployeeId=1004
select @@TRANCOUNT "Open Transaction Count"
commit tran
select @@TRANCOUNT "Open Transaction Count"
select * from tblEmployee

begin transaction transOps
update tblEmployee
set city='California'
where EmployeeId=1004
select * from tblEmployee
where EmployeeId=1004
rollback tran
select * from tblEmployee
where EmployeeId=1004

begin transaction tranOperations
	insert into tblDepartment 
	values(10,'R&D','BLR')
	save transaction EmpOps	--savepoint
	update tblEmployee
	set city='California'
	where EmployeeId=1004 

	delete from tblEmpReplica 
	where empId=1004

	rollback transaction EmpOps
	delete from tblEmployee where EmployeeId=1008
commit

select * from tblEmployee
select * from tblEmpReplica
select * from tblDepartment

--triggers
--after trigger
create or alter trigger trg_InsertDept
on tblDepartment
after delete
as
--print 'Operation performrd on Department table'
declare @name varchar(20)
select @name=deleted.DepartmentName from deleted
print @name + ' is deleted'

insert into tblDepartment values (8,'Department2','Location2')
delete from tblDepartment where DepartmentId=7
--magical tables (inserted table and deleted table)

create table tblDeptMaster
(
	Id int identity primary key,
	DeptId int,
	DeptName varchar(20),
	Loc varchar(20),
	ActionPerformed varchar(max)
)
select * from tblDeptMaster

create trigger trgDeptEntry
on tblDepartment
after insert
as
begin

	declare @id int,@name varchar(20),@loc varchar(20),@action varchar(max)
	select @id=i.departmentid,@name=i.departmentname,@loc=i.DepartmentLoc from inserted i
	set @action='Insert is performed on department table'
	
	insert into tblDeptMaster values(@id,@name,@loc,@action)
end
select * from tblDepartment
select * from tblDeptMaster
insert into tblDepartment values(7,'Dept1','Loc1')

--instead of trigger
create or alter trigger trg_PreventDeleteOnEmployeeReplica
on tblEmpReplica
instead of delete
as
declare @id int,@name varchar(20)
select @id=deleted.empid,@name=deleted.empname
from deleted
print 'Delete was attempted for :'+cast(@id as varchar) + '-'+@name

select * from tblEmpReplica

select getdate()

delete from tblEmpReplica where empid=1007