create table tblEmpReplica
(
	EmpId int not null,
	EmpName varchar(20),
	City varchar(20),
	MgrId int,
	Sal numeric(10,2),
	DeptId int,
	PhNo bigint
)

insert into tblEmpReplica
select * from tblEmployee

select * from tblEmpReplica

create unique clustered index idx_EmpId
on tblEmpReplica(EmpId)

--can I create a primary key now?
alter table tblEmpReplica
add constraint pk_EmpId primary key(EmpId)

create unique nonclustered index nidx_MgrIdPhNo
on tblEmpReplica(MgrId,PhNo)

--views
create or alter view vw_EmployeesFromMelbourne
with encryption
as
select EmployeeId,employeename,city,salary,managerid,departmentid 
from tblEmployee
where city='Melbourne'

select * from vw_EmployeesFromMelbourne

exec sp_helptext 'vw_EmployeesFromMelbourne'

create or alter view vw_EmployeesFromMelbourne
with encryption
as
select EmpId,empname,city,sal,mgrid,PhNo 
from tblEmpReplica
where city='Melbourne'

alter table tblEmpReplica
drop column deptid

select * from tblEmpReplica

select * from vw_EmployeesFromMelbourne

create or alter view vw_EmployeesFromMelbourne
with schemabinding
as
select EmpId,empname,city,sal,mgrid,PhNo 
from dbo.tblEmpReplica
where city='Melbourne'

alter table tblEmpReplica
drop column sal

update vw_EmployeesFromMelbourne
set sal=670000
where empid=1011

--stored procedures
create procedure usp_GetAllEmployees
as
select * from tblEmployee

execute usp_GetAllEmployees
exec usp_GetAllEmployees
usp_GetAllEmployees

create or alter proc usp_GetEmployeesFromCity
(@city varchar(30),@deptid int)
as
	select * from tblEmployee
	where city=@city and DepartmentId=@deptid

exec usp_GetEmployeesFromCity 'Melbourne',1
exec usp_GetEmployeesFromCity 'California',2

create or alter proc usp_AddEmployee
(@name varchar(20),@mgrid int,@sal numeric(10,2),
@deptid int, @phno bigint,@city varchar(20)='Tokyo')
as
insert into tblEmployee(employeename,salary,ManagerId,DepartmentId,PhoneNo,city) 
values(@name,@sal,@mgrid,@deptid,@phno,@city)

exec usp_AddEmployee 'Amanada Bennett',28000.00,1006,3,9876545678
exec usp_AddEmployee 'Mezel Bennett',22000.00,1001,1,9876545679,'California'
select * from tblEmployee


create or alter proc usp_CountByDeptId(@deptid int)
as
declare @count int
	select @count=count(employeeid) from tblEmployee
	where DepartmentId=@deptid
return @count


declare @empCount int
exec @empCount=usp_CountByDeptId 4
print 'No. of employees is:'+cast(@empCount as varchar)

select * from tblEmployee
where DepartmentId=2

create or alter proc usp_GetEmployeeInfo
(@id int,@name varchar(20) output,@city varchar(20) output)
as
select @name=employeename,@city=city
from tblEmployee
where EmployeeId=@id

declare @empName varchar(20),@empCity varchar(20)
exec usp_GetEmployeeInfo 1011,@empName output,@empCity output
print @empName+ ' stays in '+@empCity 

--create a SP that takes the department id and displays the max, min and avg
--salary for that department. make sure to print in the format:
-- Department No:1 has Max Salary:20000, Min Salary: 2000 and Avg Salary:8000

