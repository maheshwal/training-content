create database FrTpDotNetDB

create database TestFrTpDb
on primary
(
name=TestDB_data,
filename='D:\Demo\TestDbData.mdf',
size=5MB
)
log on
(
name=TestDB_log,
filename='D:\Demo\TestDbData.ldf',
size=5MB
)

alter database TestFrTpDb
modify name=FrTpDemoDB

alter database FrTpDemoDB
modify file
(name=TestDB_data, size=10MB, maxsize=100MB,filegrowth=0)

drop database FrTpDemoDB

execute sp_helpdb FrTpDotNetDB

execute sp_databases

select * from sys.sysdatabases

use FrTpDotNetDB

create table tblDepartment
(
	DepartmentId int primary key,--column level constraint
	DepartmentName varchar(20),
	DepartmentLoc varchar(20)
)

select * from tblDepartment

execute sp_help 'dbo.tblDepartment'
select * from sys.tables

alter table tblDepartment
add DepartmentHead varchar(20) not null

alter table tblDepartment
alter column departmenthead varchar(40) not null

exec sp_rename 'dbo.tblDepartment.DepartmentHead','HOD','column'

alter table tblDepartment
drop column HOD

insert into tblDepartment values(1,'Human Resources','Pune')
insert into tblDepartment(DepartmentId,DepartmentLoc,DepartmentName)
values(2,'HYD','Administration')
insert into tblDepartment values
(3,'Finance','GNR'),(4,'Accounting','Noida')

select * from tblDepartment
insert into tblDepartment values(5,null,null)

update tblDepartment
set DepartmentName='Research',DepartmentLoc='BLR'
where DepartmentId=5

delete from tblDepartment
where DepartmentId=5

create table tblEmployee
(
	EmployeeId int identity(1001,1),
	EmployeeName varchar(20) not null,
	City varchar(20) default('Tokyo'),
	ManagerId int,
	Salary numeric(10,2) not null,
	DepartmentId int,
	PhoneNo bigint unique,
	constraint pk_EmployeeId primary key(EmployeeId),
	constraint fk_EmpId_DepId foreign key(DepartmentId) 
	references tblDepartment(DepartmentId)
	on delete cascade on update cascade
)
alter table tblEmployee
add constraint chk_City 
check (City in ('Tokyo','Melbourne','California','Los Angeles'))

select * from tblEmployee

insert into tblEmployee(EmployeeName,City,ManagerId,Salary,DepartmentId,PhoneNo) 
values
('John Smith','California',null,80000.00,1,95854756547)

insert into tblEmployee values('Jenna Smith','Melbourne',1001,55000.00,1,null)

insert into tblEmployee 
values('Mary Jane','Los Angeles',1002,25000.00,2,9874578945)


insert into tblEmployee(EmployeeName,City,Salary,ManagerId,DepartmentId,PhoneNo)
values
('Franklin Wong','Tokyo',25000,1002,1,0866307632),
('Alicia Zelaya','Melbourne',30000,1001,4,0866307633),
('Ramesh Narayan','California',25000,1004,2,0866307634),
('Jennifer Wallace','Los Angeles',35000,1001,null,0866307635),
('Joyce English','Tokyo',80000,1001,null,0866307636),
('Ahmad Jabbar','Melbourne',75000,1004,3,0866307637),
('James Borg','Melbourne',65000,1005,4,0866307638)

select * from tblDepartment
select * from tblEmployee

select employeename,city,salary from tblEmployee

select employeename as "Name",city City,salary "Income" from tblEmployee

select employeename +' stays in '+city from tblEmployee

select employeename,city,salary from tblemployee 
order by salary desc

select top 3 employeename,city,salary from tblemployee 
order by salary desc

select employeeid,employeename,city,salary 
from tblEmployee
where city='Tokyo'

select employeeid,employeename,city,salary 
from tblEmployee
where Salary<=30000 or salary>=40000

select employeeid,employeename,city,salary 
from tblEmployee
where Salary>=30000 and salary<=40000

select employeeid,employeename,city,salary 
from tblEmployee
where Salary between 30000 and 40000

select employeeid,employeename,city,salary 
from tblEmployee
where city in ('Tokyo','Los Angeles','Melbourne')

select employeeid,employeename,city,salary 
from tblEmployee
where employeename like 'j%'

select employeeid,employeename,city,salary 
from tblEmployee
where employeename like '_e%'

select employeeid,employeename,city,salary 
from tblEmployee
where EmployeeName like '%nn%'

select * from tblEmployee

select employeename from tblEmployee
where ManagerId is null

select employeename,DepartmentId from tblEmployee
where DepartmentId is not null

