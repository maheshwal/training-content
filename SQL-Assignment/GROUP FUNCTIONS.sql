
-------------------------- GROUP FUNCTIONS ----------------------------

use EmployeeDB

select min(LastName) from Employees
group by LastName

select LastName from Employees order by LastName desc

select count(*) as 'Total Rows' from Employees