
-----------------------------JOINS---------------------------------

use EmployeeDB
select * from Employees

-- a)
select emp.Firstname, emp.LastName,emp.DepartmentID, dept.Departmentname
from Employees emp
 join Departments dept
on emp.DepartmentID=dept.DepartmentID

--2
select firstname,lastname,Employees.DepartmentID ,departmentName from Employees join Departments on Employees.DepartmentID=Departments.DepartmentID 
where Departments.DepartmentID=2 or Departments.DepartmentID=3
--3
select * from  Departments
select * from  Locations
select DepartmentName,City,stateprovince from Locations join Departments on Departments.LocationID=Locations.LocationID
--4
select firstname,lastname,departmentname,city,StateProvince from Employees join Departments on Departments.DepartmentID=Employees.DepartmentID join locations on Locations.LocationID=Departments.LocationID
--5
select concat(firstname,' ',lastname) fullname ,departmentname,city,StateProvince from Employees 
join Departments on Departments.DepartmentID=Employees.DepartmentID  
join Locations on Locations.LocationID=Departments.LocationID where LastName like'%a%' 

--------------subqueries----------
--1
select firstname ,salary from Employees where Salary > (select salary from Employees where EmployeeID=102)


--2
select departmentId,departmentName from departments where LocationID=(select LocationId from Departments where DepartmentID=333)
select * from Departments
--3
select lastname,hiredate from Employees where HireDate>(select HireDate from Employees where  EmployeeID=1005)

------------------------------procedures-------------------------------------------------------------


select * from Locations
select * from Departments
select * from Employees

create or alter proc usp_DisplayCity(@city varchar(20))
as
	select DepartmentName,Locations.LocationID from Departments join   Locations on 
	 Departments.LocationID=Locations.LocationID where city=@city
	 exec usp_DisplayCity 'jodhpur'

/*b. Display the employees whose salary matches the salary of employee no specified*/
update Employees
set Salary=35000 where EmployeeID=1004
create or alter proc usp_displayEmp(@EmployeeId int)
as
     select FirstName from employees where Salary=(select salary from Employees where EmployeeID=@EmployeeId)
	 exec usp_displayEmp 1001
/*c. Display Jobs with supplied value for Min and Max salary*/
exec sp_help 
create proc jobbyminmaxsal(@Minsal Decimal,@Maxsal decimal)
As
select * from jobs where Minsalary=@Minsal And Maxsalary=@Maxsal
exec jobbyminmaxsal @minsal=35000 ,@Maxsal=55000
