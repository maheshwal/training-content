
-------------------------- SELECT QUERY ------------------------------
use EmployeeDB

-- a)
		select * from Employees 
-- b)
		Select LastName "last_name",FirstName "first_name" from Employees
-- c)
		Select DepartmentName,DepartmentID,ManagerID as "MNG" From Departments
-- d)
		--Above is False
			Select DepartmentName as "department_name" FROM Departments
-- e)
		Select LastName "last_NAME",FirstName "fiRST_NamE" from Employees
-- f)
		Select EmployeeID,FirstName,LastName,Phone,DepartmentID
		from Employees
-- g)
		Select FirstName,LastName,HireDate,Salary,(Salary+(0.2*Salary)) as "ANNUAL_SAL"
        from Employees
-- h)
		
		SELECT CONCAT(FirstName,' ',LastName) as "FULL_NAME",
		CONCAT(Email,'-',Phone) as "CONTACT_DETAILS" 
		FROM Employees
-- i)
		Select Distinct ManagerID from Employees
-- j)
		SELECT CONCAT(LastName,' ',JobID) as "EMPLOYEE_AND_TITLE" 
		from Employees
-- k)
		SELECT FirstName "FN",LastName "LN",Salary "SAL",CONCAT(HireDate,' ','HD')
		FROM Employees
-- l)
		SELECT DISTINCT Salary FROM Employees
-- m)
		SELECT DISTINCT DepartmentID,JobID FROM Employees
