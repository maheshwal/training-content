
------------------------- DDL STATEMENTS ------------------------
Create DATABASE EmployeeDB

USE EmployeeDB

CREATE TABLE Employees(
	EmployeeID int primary key,
	FirstName varchar(20),
	LastName varchar(20),
	Email varchar(20), 
	Phone BIGINT, 
	HireDate date, 
	JobID int, 
	Salary decimal(10,2), 
	CommissionPct numeric(6,2), 
	ManagerID int, 
	DepartmentID int
)

CREATE TABLE Departments(
	DepartmentID int Primary Key, 
	DepartmentName varchar(20),
	ManagerID int,
	LocationID int
	)
	

CREATE TABLE Locations(
	LocationID int Primary Key, 
	StreetAddress varchar(20),
	PostalCode BIGINT, 
	City varchar(20), 
	StateProvince varchar(20), 
	CountryID int
	)

CREATE TABLE Countries(
	CountryID int Primary Key, 
	CountryName varchar(20), 
	RegionID int
	)
	

CREATE TABLE Regions(
	RegionID int Primary Key,
	RegionName varchar(20)
)

CREATE TABLE Jobs(
	JobID int Primary Key, 
	JobTitle varchar(20), 
	MinSalary decimal(10,2), 
	MaxSalary decimal(10,2)
)
CREATE TABLE JobHistory(
	EmployeeID int, 
	StartDate date, 
	EndDate date, 
	JobID int, 
	DepartmentID int
)

------------------------ CONSTRAINTS -------------------------------------
alter table Departments
add constraint fk_loc_dept foreign key(LocationID)
references Locations(LocationID)

alter table Employees
add constraint fk_dept_emp foreign key(DepartmentID)
references Departments(DepartmentID)

alter table Locations
add constraint fk_ctry_loc foreign key(CountryID)
references Countries(CountryID)

alter table JobHistory
add constraint fk_job_jbhstry foreign key(JobID)
references Jobs(JobID)

alter table JobHistory
add constraint fk_dept_jbhstry foreign key(DepartmentID)
references Departments(DepartmentID),
constraint fk_emp_jbhstry foreign key(EmployeeID)
references Employees(EmployeeID)

alter table Countries
add constraint fk_region_ctry foreign key(RegionID)
references Regions(RegionID)

alter table JobHistory
add unique(EmployeeID, DepartmentID)

--drop database EmployeeDB
--use BookStoreDB


------- Inserting values -----------------------

select * from Employees
insert into Employees values(101,'Sritam','Das','sri@g.com',70018,'2021-05-14',1001,68000.00,456,501,1)
insert into Employees values(102,'Priyanka','Choudhury','pry@g.com',56891,'2021-05-21',1002,45000.00,458,502,2)
insert into Employees values(103,'Urmi','Dutta','urmi@g.com',78577,'2021-05-21',1001,51000.00,456,501,1)
select * from Departments
insert into Departments values(3,'Finance',502,25)

select * from Locations
insert into Locations values(21,'Baker Street',7885,'London','UK',755)
insert into Locations values(25,'HYD',4557,'HYD','India',755)
select * from Countries
insert into Countries values(755,'UK',15)

select * from Regions
insert into Regions values(15,'Europe')