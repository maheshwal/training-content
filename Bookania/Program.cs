﻿using Bookania.BLL;
using System;

namespace Bookania
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PublisherLogics publisher = new PublisherLogics();
            UserLogics user = new UserLogics();
            //publisher.PublisherRegister();
           // user.UserRegister();
            user.UserLogin();
            //publisher.PublisherLogin();
            user.DisplayAllBooks();
            //user.DisplayBookByAuthor();
            //user.DisplayBookByPublisher();
            //user.DisplayBookByISBN();
            //user.DisplayBookByTitle();
            //user.DisplayBookByCategory();
            user.MultipleQuery();

            Console.ReadLine();
        }
    }
}
