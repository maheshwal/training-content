﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookania.Interfaces
{
    interface IPublisherLogics
    {
        void PublisherRegister();
        void PublisherLogin();
        void AddBookByPublisher();
        void DeleteBookByPublisher();
    }
}
