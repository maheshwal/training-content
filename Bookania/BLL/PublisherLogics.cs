﻿
using Bookania.Interfaces;
using Bookania.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bookania.Connection;

namespace Bookania.BLL
{
    public class PublisherLogics : IPublisherLogics
    {


        public void PublisherRegister()
        {
            using BookaniaContext publisher = new BookaniaContext();
            Console.WriteLine("Enter The Publisher Name");
            string PName = Console.ReadLine();
            Console.WriteLine("Enter The Publisher Contact Number");
            int PContact = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter The Password");
            string PPassword = Console.ReadLine();
            Publisher publisherObj = new Publisher()
            {
                PublisherName = PName,
                PublisherContact = PContact,
                Password = PPassword,
            };
            publisher.Publishers.Add(publisherObj);
            publisher.SaveChanges();
        }

        /*
        static void getBook()
        {
            using (var getbook = new BooksDBContext())
            {
                foreach (var item in getbook.Books.ToList())
                {
                    Console.WriteLine("Id:{0}", item.BookId + "Title " + item.Title);
                }
            }
        }
        */
        public void DisplayBooksOfPublisher()
        {
            using (var publish = new BookaniaContext())
            {
                var booksOfPublisher = from book1 in publish.Books
                                       join publisher1 in publish.Publishers on book1.PublisherId equals publisher1.Id
                                       select new
                                       {
                                           BookTitle = book1.Title,
                                           BookISBN = book1.ISBN,
                                           BookPrice = book1.Price
                                       };
                foreach (var item in booksOfPublisher)
                {
                    Console.WriteLine(item.BookTitle + "" + item.BookISBN + "" + item.BookPrice);
                }
            }
        }
        public void PublisherLogin()
        {
            using (var publish = new BookaniaContext())
            {
                Console.WriteLine("Enter Your Publisher Name");
                string PName = Console.ReadLine();
                Console.WriteLine("Enter Your Password");
                string PPassword = Console.ReadLine();
                var play = (from publisherObj in publish.Publishers
                            where publisherObj.PublisherName == PName && publisherObj.Password == PPassword
                            select publisherObj).FirstOrDefault();
                Console.WriteLine(play);

                if (play != null)
                {
                    DisplayBooksOfPublisher();
                }
                //FirstOrDefault
                // Create data context  
                //using BookaniaContext publisher = new BookaniaContext();
                // DataContext db = new DataContext(connString);
                // Create typed table  
                // Table<Publisher> publisher1 = publisher.GetTable<Publisher>();
                // Query database  
                /* var contactDetails =
                    from c in contacts
                    where c.Title == "Mr."
                    orderby c.FirstName
                    select c;*/




                /* var books = (from bookObj in books1
                              where bookObj.PublisherId == PName && publisherObj.Password == PPassword
                             select publisherObj).FirstOrDefault();*/
            }



            /* var LoggedPublisher = from publisherObj in Publisher
                                   where publisherObj.PublisherName==PName && publisherObj.Password==PPassword
                                  select publisherObj;
             foreach (Publisher item in LoggedPublisher)
             {
                 Console.WriteLine(item.Name + " " + item.Age);
             }*/
        }

        public void AddBookByPublisher()
        {
            using (var addBookByPublisher = new BookaniaContext())
            {
                Console.WriteLine("Enter the ISBN Number Of Book");
                int isbn = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter the Title Of Book");
                string bookTitle = Console.ReadLine();
                Console.WriteLine("Enter the Price Of Book");
                double boookPrice = Convert.ToDouble(Console.ReadLine());
                Book newBook = new Book()
                {
                    ISBN = isbn,
                    Title = bookTitle,
                    Price = boookPrice
                };
                addBookByPublisher.Books.Add(newBook);
                addBookByPublisher.SaveChanges();
            }
            DisplayBooksOfPublisher();
            // throw new NotImplementedException();
        }

       public void DeleteBookByPublisher()
        {
            Console.WriteLine("Enter the Title of Book Which You Want to Delete");
            string bookTitle = Console.ReadLine();
            using (var deleteBookByPublisher = new BookaniaContext())
            {
                Book book = new Book()
                {
                    Title = bookTitle
                };
                deleteBookByPublisher.Books.Remove(book);
                deleteBookByPublisher.SaveChanges();
            }
               // throw new NotImplementedException();
        }

        
    }

}



