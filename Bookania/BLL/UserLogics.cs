﻿using Bookania.Connection;
using Bookania.Interfaces;
using Bookania.Models;
using System;
using System.Linq;



namespace Bookania.BLL
{
    public class UserLogics : IUserLogics
    {
        public UserLogics()
        {

        }
        public void UserRegister()
        {
            using BookaniaContext user = new BookaniaContext();
            Console.WriteLine("Enter The User Name");
            string UName = Console.ReadLine();
            Console.WriteLine("Enter The Email");
            string UEmail = Console.ReadLine();
            Console.WriteLine("Enter The Password");
            string UPassword = Console.ReadLine();
            User userObj = new User()
            {
                UserName = UName,
                Email = UEmail,
                Password = UPassword,
                RoleId = 2
            };
            user.Users.Add(userObj);
            user.SaveChanges();
        }

        public void UserLogin()
        {
            using (var users = new BookaniaContext())
            {
                Console.WriteLine("Enter Your User Name");
                string UName = Console.ReadLine();
                Console.WriteLine("Enter Your Password");
                string UPassword = Console.ReadLine();
                var user = (from userObj in users.Users
                            where userObj.UserName == UName && userObj.Password == UPassword
                            select userObj).FirstOrDefault();

                if (user != null)
                {
                    DisplayAllBooks();
                }

            }
        }

        public void DisplayBookByISBN()
        {

            using (BookaniaContext db = new BookaniaContext())
            {
                var book = from b in db.Books
                           join a in db.Authors on b.AuthorId equals a.Id
                           select new
                           {
                               ISBN = b.ISBN,
                               BookName = b.Title,
                               AuthorName = b.author.AuthorName,
                               BookPrice = b.Price,
                               BookPublisher = b.publisher.PublisherName
                           };
                foreach (var t in book)
                {
                    Console.WriteLine("{0} {1} {2} {3} {4}", t.ISBN, t.BookName, t.AuthorName, t.BookPublisher, t.BookPrice);
                }
                Console.WriteLine("enter ISBN:");
                int id = Convert.ToInt32(Console.ReadLine());
                // var books = from bookObj in db.Books.Join().Where(b => b.ISBN == id) select bookObj;
                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.ISBN == id
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine("ISBN:{0}     |       Author Name:{1}     |       Publisher Name:{2}      |    Price:{3}", b.ISBN, b.AuthorName, b.PublisherName, b.Price);
                }
            }

        }
        public void DisplayBookByTitle()
        {

            using (BookaniaContext db = new BookaniaContext())
            {
                var book = from b in db.Books
                           join a in db.Authors on b.AuthorId equals a.Id
                           select new
                           {
                               ISBN = b.ISBN,
                               BookName = b.Title,
                               AuthorName = b.author.AuthorName,
                               BookPrice = b.Price,
                               BookPublisher = b.publisher.PublisherName
                           };
                foreach (var t in book)
                {
                    Console.WriteLine("ISBN:{0}     |Title:{1}      | Author:{2}        | Publisher:{3}     |Price:{4}", t.ISBN, t.BookName, t.AuthorName, t.BookPublisher, t.BookPrice);
                }
                Console.WriteLine("enter Title :");
                string title = Console.ReadLine();

                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.Title == title
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine("ISBN:{0}     |       Author Name:{1}     |       Publisher Name:{2}      |    Price:{3}", b.ISBN, b.AuthorName, b.PublisherName, b.Price);
                }
            }

        }
        public void DisplayBookByAuthor()
        {
            using (BookaniaContext db = new BookaniaContext())
            {
                var author = (from bookObj in db.Authors orderby bookObj.Id select bookObj).ToList();


                foreach (var a in author)
                {
                    Console.WriteLine("ID:{0} author Name :{1}", a.Id, a.AuthorName);
                }

                Console.WriteLine("enter the Author ID:");
                int id = Convert.ToInt32(Console.ReadLine());
                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join c in db.Categories on b.CategoryId equals c.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.AuthorId == id
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Category = c.CategoryName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine(" ISBN:{0}        |       Publisher Name:{1}      |       Category :{2}       |       Price:{3}", b.ISBN, b.PublisherName, b.Category, b.Price);
                }
            }

        }
        public void DisplayBookByPublisher()
        {
            using (var db = new BookaniaContext())
            {
                //var publishers= (from bookObj in publish.Publishers select bookObj).ToList();
                var publishers = (from bookObj in db.Publishers orderby bookObj.Id select bookObj).ToList();


                foreach (var p in publishers)
                {
                    Console.WriteLine("ID:{0} Publisher Name:{1}", p.Id, p.PublisherName);
                }
                Console.WriteLine("enter publisher ID:");
                int id = Convert.ToInt32(Console.ReadLine());
                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join c in db.Categories on b.CategoryId equals c.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.PublisherId == id
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Category = c.CategoryName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine(" ISBN:{0}        |       Author Name:{1}      |       Category :{2}       |       Price:{3}", b.ISBN, b.AuthorName, b.Category, b.Price);
                }
            }
        }
        public void DisplayBookByCategory()
        {
            using (var db = new BookaniaContext())
            {
                var category = (from bookObj in db.Categories orderby bookObj.Id select bookObj).ToList();


                foreach (var c in category)
                {
                    Console.WriteLine("ID:{0} categories :{1}", c.Id, c.CategoryName);
                }
                Console.WriteLine("enter category ID:");
                int id = Convert.ToInt32(Console.ReadLine());
                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join c in db.Categories on b.CategoryId equals c.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.CategoryId == id
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Category = c.CategoryName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine(" ISBN:{0}        |       Author Name:{1}      |       Publisher :{2}       |       Price:{3}", b.ISBN, b.AuthorName, b.PublisherName, b.Price);
                }
            }
        }
        /*
        public void MultipleQuery()
        {
            var dataset = new BookaniaContext();
            string query = "from b in dataset.Books join a in dataset.Authors on b.AuthorId equals a.Id join c in dataset.Categories on b.CategoryId equals c.Id join p in dataset.Publishers on b.PublisherId equals p.Id";

            using (var db = new BookaniaContext())
            {
                Console.WriteLine("Enter your choice : 1.Search by Category 2.Search by Author 3.Search by Publisher 4.Search by Title");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        var dat = query;
                        var cat = (from bookObj in db.Categories orderby bookObj.Id select bookObj).ToList();
                        foreach (var c in cat)
                        {
                            Console.WriteLine("ID:{0} categories :{1}", c.Id, c.CategoryName);
                        }
                        Console.WriteLine("enter category ID:");
                        int v = Convert.ToInt32(Console.ReadLine());
                        query = query + (" where b.CategoryId=={0}", v);
                        Console.WriteLine("Do you want to add any other contstraint? 1.Author 2.Publisher 3.Title");
                        int choice1 = Convert.ToInt32(Console.ReadLine());
                        switch (choice1)
                        {
                            case 1:
                                var auth = (from bookObj in db.Authors orderby bookObj.Id select bookObj).ToList();


                                foreach (var a in auth)
                                {
                                    Console.WriteLine("ID:{0} Author :{1}", a.Id, a.AuthorName);
                                }
                                Console.WriteLine("enter author ID:");
                                int auth1 = Convert.ToInt32(Console.ReadLine());
                                query += (" && b.AuthorId=={0}", auth1);
                                dat.ToList() + select new
                                {
                                    ISBN = b.ISBN,
                                    AuthorName = a.AuthorName,
                                    PublisherName = p.PublisherName,
                                    Category = c.CategoryName,
                                    Price = b.Price
                                };
                                foreach (var b in books)
                                {
                                    Console.WriteLine(" ISBN:{0}        |       Author Name:{1}      |       Publisher :{2}       |       Price:{3}", b.ISBN, b.AuthorName, b.PublisherName, b.Price);
                                }
                                break;

                            default:
                                break;
                        }

                        break;
                    case 2:

                        break;
                    default:
                        break;
                }
                


                var category = (from bookObj in db.Categories orderby bookObj.Id select bookObj).ToList();


                foreach (var c in category)
                {
                    Console.WriteLine("ID:{0} categories :{1}", c.Id, c.CategoryName);
                }
                Console.WriteLine("enter category ID:");
                int id = Convert.ToInt32(Console.ReadLine());
                var books = from b in db.Books
                            join a in db.Authors on b.AuthorId equals a.Id
                            join c in db.Categories on b.CategoryId equals c.Id
                            join p in db.Publishers on b.PublisherId equals p.Id
                            where b.AuthorId == id
                            select new
                            {
                                ISBN = b.ISBN,
                                AuthorName = a.AuthorName,
                                PublisherName = p.PublisherName,
                                Category = c.CategoryName,
                                Price = b.Price
                            };
                foreach (var b in books)
                {
                    Console.WriteLine(" ISBN:{0}        |       Author Name:{1}      |       Publisher :{2}       |       Price:{3}", b.ISBN, b.AuthorName, b.PublisherName, b.Price);
                }
            }
        }
        
        public void MultipleQuery1()
        {
            var dataset = new BookaniaContext();
            string query = "from b in dataset.Books join a in dataset.Authors on b.AuthorId equals a.Id join c in dataset.Categories on b.CategoryId equals c.Id join p in dataset.Publishers on b.PublisherId equals p.Id";
            var books1 = query.ToList();

            
              
            
        }
        */



        public void DisplayAllBooks()
        {
            using (BookaniaContext db = new BookaniaContext())
            {
                var book = from b in db.Books
                           join a in db.Authors on b.AuthorId equals a.Id
                           select new
                           {
                               BookTitle = b.Title,
                               Author = b.author.AuthorName,
                               BookPrice = b.Price,
                               BookPublisher = b.publisher.PublisherName
                           };
                foreach (var t in book)
                {
                    Console.WriteLine("Book Title:{0}    Author:{1}      Publisher:{2}        Price:{3}", t.BookTitle, t.Author, t.BookPublisher, t.BookPrice);
                }
            }

        }


        /*

                //3rd helping method
                public Book DisplayBooks()
                {
                    Book book;
                    using (BookaniaContext db = new BookaniaContext())
                    {
                         book = from b in db.Books
                                      select b;


                    }

                    return book;

                }

          */


        //3rd

        /*
        public void  OrderBook()
        {
            DisplayAllBooks();


            Console.WriteLine("Enter the Id of book u want to order");
                int bookId = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter the Quantity u want to purchase");
                int quantity = int.Parse(Console.ReadLine());
                //code required here for multiple orders
                Book book = from b in Book
                            where b.Id == bookId
                            select b;

                Order orders = new Order();
                orders.Date = DateTime.Now;
                orders.BookId = book.Id;
                orders.Quantity = quantity;
                orders.book = book;
                //orders.user = User;
            
            //return "your order is booked sucessfully";
        }
        */

        public void OrderBook(User user)
        {
            Order orderDetails = new Orders();
            Order order = new Order();
           // order.UserId = user.Id;
            order.Date= DateTime.Now;
            order.PaymentStatus = "Pending";
            order.Payment = 0;
            _context.Orders.Add(order);
            _context.SaveChanges();
            DisplayBook();
            int Bookid;
            int Quantity;
            System.Collections.Generic.List<Book> books;
            while (true)
            {
                orderDetails.OrderId = order.OrderId;
                Console.WriteLine("Select the Id of the Book which you want to order:- ");

                Bookid = int.Parse(Console.ReadLine());



                Console.WriteLine("Enter The Book Quantity:- ");

                Quantity = int.Parse(Console.ReadLine());
                var bookList = (from bookObj in _context.BookStock
                                where bookObj.BookId == Bookid && bookObj.Quantity >= Quantity
                                select bookObj).First();
                if (bookList != null)
                {
                    bookList.Quantity = bookList.Quantity - Quantity;
                    _context.BookStock.Update(bookList);
                    _context.SaveChanges();
                    orderDetails.BookId = Bookid;
                    orderDetails.Quantity = Quantity;
                    orderDetails.Price = (from obj in _context.Books
                                          where obj.BookId == Bookid
                                          select obj.Price).FirstOrDefault();
                    _context.OrderDetails.Add(orderDetails);
                    _context.SaveChanges();


                    double totalPrice = 0;
                    var orderBookTotalPrice = from obj in _context.OrderDetails
                                              where obj.OrderDetailsId == orderDetails.OrderDetailsId
                                              select obj;
                    foreach (var obj1 in orderBookTotalPrice)
                    {
                        totalPrice += (obj1.Quantity * obj1.Price);

                    }
                    var orderToUpdatePrice = _context.Orders.Find(order.OrderId);
                    orderToUpdatePrice.TotalPrice = totalPrice;
                    _context.Orders.Update(orderToUpdatePrice);
                    _context.SaveChanges();
                }
                else
                {
                    Console.WriteLine("That much book quantity is not available");
                }
                Console.WriteLine("You want To Add More Books Press Y. ");
                Console.WriteLine("if not Press N.");
                var Option = Console.ReadLine();
                if (Option == "N" || Option == "n")
                {
                    break;
                }
            }

        }

        //4th


        public void UpdatePaymentStatus()
        {
            Console.WriteLine("Payment Type is Cash on delivery");
            Order orders = new Order();
            orders.PaymentStatus = "Recived";
        }

    
    
    
    
    }
}
