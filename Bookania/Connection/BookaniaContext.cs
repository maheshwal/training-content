﻿using Bookania.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookania.Connection
{
   public class BookaniaContext : DbContext
    {
        public BookaniaContext()
        {

        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("appsetting.json").Build();
            // var optionBuilder = new DbContextOptionsBuilder<BookaniContext>();
            optionsBuilder.UseSqlServer(configuration["ConnectionStrings:BookaniContext"]);
         
        }
       
    }
}
