﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookania.Models
{
   public  class Publisher
    {

        [Key]
        public int Id { get; set; }
        [Required]
        public string PublisherName { get; set; }
        public int PublisherContact { get; set; }
        [Required]
        public string Password { get; set; }

    }
}
