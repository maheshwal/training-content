﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookania.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int ISBN { get; set; }
        [Required]
        public string Title { get; set; }
        public double Price { get; set; }


        public int PublisherId { get; set; }
        public Publisher publisher { get; set; }

        public int CategoryId { get; set; }
        public Category category { get; set; }

        public int AuthorId { get; set; }
        public Author author { get; set; }


    }
}
