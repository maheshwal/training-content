﻿using System.ComponentModel.DataAnnotations;

namespace Bookania.Models
{
    public class Author
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string AuthorName { get; set; }
        public int AuthorContact { get; set; }

    }
}
