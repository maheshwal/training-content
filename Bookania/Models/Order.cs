﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookania.Models
{
   public class Order
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Payment { get; set; }
        public int Quantity { get; set; }

        public int BookId { get; set; }
        public Book book { get; set; }
        public User user { get; set; }
     public string PaymentStatus { get; set; }
    
    }
}
