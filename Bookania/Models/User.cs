﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookania.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public bool Active { get; set; }

        public int BookId { get; set; }
        public Book book { get; set; }

        public int RoleId { get; set; }
        public Role role { get; set; }

    }
}
