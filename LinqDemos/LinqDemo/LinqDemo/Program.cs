﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqDemo
{
    delegate bool FindPeople(Person personObj);
    class Program
    {

        static void Main(string[] args)
        {
            City[] cities =
            {
                new City{CityID=1,CityName="Melbourne"},
                new City{CityID=2,CityName="Los Angeles"},
                new City{CityID=3,CityName="Las Vegas"},
                new City{CityID=4,CityName="New York"}
            };

            Person[] people = 
            {
                new Person{SSN=100,Name="John Smith",Age=12,CityID=1},
                new Person{SSN=101,Name="Jenna Smith",Age=13,CityID=2},
                new Person{SSN=102,Name="Jordan Sanders",Age=22,CityID=4},
                new Person{SSN=103,Name="Mark Dawson",Age=32,CityID=3},
                new Person{SSN=104,Name="Melisa Dawson",Age=15,CityID=1},
                new Person{SSN=105,Name="Mark Benson",Age=16,CityID=4},
                new Person{SSN=106,Name="Zara Panchmi",Age=19,CityID=2},
                new Person{SSN=107,Name="Victoria Francis",Age=42,CityID=3},
                new Person{SSN=108,Name="Mary Jane",Age=16,CityID=2},
                new Person{SSN=109,Name="Tony Stark",Age=24,CityID=1},
                new Person{SSN=110,Name="Robert Aniston",Age=30,CityID=1}
            };
            //write a foreach loop that gets me the teenagers
            Console.WriteLine("List of teenagers:");
            #region Why_LINQ
            /*foreach(Person personObj in people)
            {
                if(personObj.Age>12 && personObj.Age<20)
                {
                    Console.WriteLine("{0}:{1}",personObj.Name,personObj.Age);
                }
            }*/

            //using delegate
            /*Person[] teenager = PersonExtension.where(people, delegate (Person personObj)
               {
                   return personObj.Age > 12 && personObj.Age < 20;
               });
            Person[] peopleTypes = PersonExtension.where(people, delegate (Person personObj)
               {
                   return personObj.SSN == 103;
               });
            Person[] peopleNames = PersonExtension.where(people, delegate (Person personObj)
            {
                return personObj.Name =="Mary Jane";
            });

            //method syntax of LINQ
            Person searchedPerson = people.Where(personObj => personObj.Name == "Mary Jane")
                .FirstOrDefault();*/
            #endregion

            //Query Syntax
            #region Query_Syntax
            /* List<string> FruitsList = new List<string>
             {
                 "Strawberry","Blueberry","Mulberry","Raspberry","Blackberry","Cherry","Pineapple"
             };


             var berryFruits = from fruit in FruitsList
                               where fruit.Contains("berry")
                               select fruit;
             Console.WriteLine("Fruis with berry:");
             foreach(string fruit in berryFruits)
             {
                 Console.WriteLine(fruit);
             }

             var teenagers = from personObj in people
                             where personObj.Age > 12 && personObj.Age < 20
                             select personObj;
             Console.WriteLine("------------------------------------------");
             Console.WriteLine("Teenagers are:");
             foreach(Person p in teenagers)
             {
                 Console.WriteLine(p.Name+":"+p.Age);
             }*/
            #endregion
            //extension methods
            /*Console.WriteLine( ExtensionClass.FirstNoIsGreaterThanSecondNo(100, 20) ) ;
            string str1 = "Maria", str2 = "Collins";
            Console.WriteLine(ExtensionClass.ConcatStringsWithSpace(str1,str2));*/
            //IEnumerable or IQueryable
            #region Method_Syntax
            //Method Syntax
            /*List<string> FruitsList = new List<string>
             {
                 "Strawberry","Blueberry","Mulberry","Raspberry","Blackberry","Cherry","Pineapple"
             };


            var berryFruits = FruitsList.Where(fruit => fruit.Contains("berry"));

            Console.WriteLine("Fruits with berry:");
            foreach (string fruit in berryFruits)
            {
                Console.WriteLine(fruit);
            }

            /*var teenagers = from personObj in people
                            where personObj.Age > 12 && personObj.Age < 20
                            select personObj;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Teenagers are:");
            foreach (Person p in teenagers)
            {
                Console.WriteLine(p.Name + ":" + p.Age);
            }*/
            #endregion
            #region QueryOps
            //people in their twenties
            //deffered execution
            /* Console.WriteLine("People in twenties:");
             var twentiesPeople = from personObj in people
                                  where personObj.Age >= 20 && personObj.Age < 30
                                  select personObj;
             foreach(Person p in twentiesPeople)
             {
                 Console.WriteLine(p.Name);
             }
             //immediate execution
             Console.WriteLine("People in thirties:");
             var thirtiesPeople = (from personObj in people
                                  where personObj.Age >= 30 && personObj.Age < 40
                                  select personObj).ToArray();

             foreach (Person p in thirtiesPeople)
             {
                 Console.WriteLine(p.Name);
             }
             */
            //grouping operators
            //Query syntax
            /*Console.WriteLine("---------------------Query Syntax-------------------------");
            var groupByAge = from personObj in people
                             group personObj by personObj.Age;
            foreach (var ageGroup in groupByAge)
            {
                Console.WriteLine("Age group:"+ageGroup.Key);
                
                foreach (Person p in ageGroup)
                {
                    Console.WriteLine(p.Name+":"+p.Age);
                }
                Console.WriteLine("=========================");
            }
            Console.WriteLine("---------------------Method Syntax-------------------------");
            var ageGroupForPeople = people.GroupBy(person => person.Age);
            foreach (var ageGroup in ageGroupForPeople)
            {
                Console.WriteLine("Age group:" + ageGroup.Key);

                foreach (Person p in ageGroup)
                {
                    Console.WriteLine(p.Name + ":" + p.Age);
                }
                Console.WriteLine("=========================");
            }
            */
            //method syntax

            //join
            //query syntax
            /*var peopleWithCity = from city in cities
                                 join person in people
                                 on city.CityID equals person.CityID
                                 select new
                                 {
                                     PersonName = person.Name,
                                     PersonAge = person.Age,
                                     CityName = city.CityName
                                 };
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine("People Name with city name:");
            foreach (var p in peopleWithCity)
            {
                Console.WriteLine(p.PersonName + ":" + p.CityName+":"+p.PersonAge);
            }

            //method syntax
            var peopleDataWithCityName = people.Join(cities, person => person.CityID, city => city.CityID,
                (person, city) => new
                {
                    PersonName = person.Name,
                    CityName = city.CityName
                });
            Console.WriteLine("---------------------Method Syntax-------------------------");
            Console.WriteLine("People Name with city name:");
            foreach (var p in peopleDataWithCityName)
            {
                Console.WriteLine(p.PersonName + ":" + p.CityName );
            }*/

            //ordering of data
            //query syntax
            var orderPeopleByAge = from person in people
                                   orderby person.Age
                                   select person;
            var orderPeopleByAgeDescending = from person in people
                                   orderby person.Age descending
                                             select person;
            Console.WriteLine("---------------------Query Syntax-------------------------");
            Console.WriteLine("People Name with age ascending:");
            foreach (var p in orderPeopleByAge)
            {
                Console.WriteLine(p.Name + ":" + p.Age);
            }
            Console.WriteLine("===============================================================");

            Console.WriteLine("People Name with age descending:");
            foreach (var p in orderPeopleByAgeDescending)
            {
                Console.WriteLine(p.Name + ":" + p.Age);
            }
            Console.WriteLine("*****************************************************************");

            //method syntax
            Console.WriteLine("---------------------Method Syntax-------------------------");
            var arrangePeopleInAgeAscending = people.OrderBy(person => person.Age).ThenBy(person => person.SSN);
            var arrangePeopleInAgeDescending = people.OrderByDescending(person => person.Age);
            Console.WriteLine("People Name with age and SSN ascending:");
            foreach (var p in arrangePeopleInAgeAscending)
            {
                Console.WriteLine(p.Name + ":" + p.Age+":"+p.SSN);
            }
            Console.WriteLine("===============================================================");

            Console.WriteLine("People Name with age descending:");
            foreach (var p in arrangePeopleInAgeDescending)
            {
                Console.WriteLine(p.Name + ":" + p.Age);
            }
            
            #endregion

        }
    }
}
