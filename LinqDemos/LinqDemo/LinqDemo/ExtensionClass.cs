﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqDemo
{
    static class ExtensionClass
    {
        /*
         1. Extension method should always be a static method in static class
         2. Extension method's 1st parameter will be this-keyword followed by th class to be extended
         */

        public static bool FirstNoIsGreaterThanSecondNo(this int num1,int num2)
        {
            return num1 > num2;
        }

        public static string ConcatStringsWithSpace(this string str1,string str2)
        {
            return str1 + " " + str2;
        }
    }
}
