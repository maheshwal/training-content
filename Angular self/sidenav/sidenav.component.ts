import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  collapse = false;
  ngOnInit(): void {
  }
  constructor(private route:Router){

  }
  moveToContactUs()
  {
    this.route.navigate(['app-contactus'])
  }
  toggleSidebar() {
    this.collapse = !this.collapse;
  }

}
