﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQWorkshop
{
    delegate bool FindPeople(Employee personObj);
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] people =
           {
                new Employee{ DeptID=10, Name ="Harshad", Doj = new DateTime (2014,06,30) , Sal =65000.00, Designation ="System Analyst" },
                new Employee{ DeptID=12, Name ="Anagha", Doj = new DateTime (2018,03,14) , Sal =89500.00, Designation ="Manager" },
                new Employee{ DeptID=5, Name ="Swizzle", Doj = new DateTime (2019,06,20) , Sal =78500.00, Designation ="Team Lead" },
                new Employee{ DeptID=17, Name ="Brotin", Doj = new DateTime (2012,07,6) , Sal =65421.00, Designation ="Testing Engineer" },
                new Employee{ DeptID=17, Name ="Pravin", Doj = new DateTime (2010,08,10) , Sal =78605.50, Designation ="System Analyst" },
                new Employee{ DeptID=10, Name ="Anant", Doj = new DateTime (2013,09,4) , Sal =56000.00, Designation ="Quality Analyst" },
                new Employee{ DeptID=5, Name ="Viraj", Doj = new DateTime (2019,09,3) , Sal =37500.50, Designation ="Developer" },
                new Employee{ DeptID=3, Name ="Namrata", Doj = new DateTime (2020,02,14) , Sal =42500.50, Designation ="System Analyst" },
                new Employee{ DeptID=17, Name ="Mitali", Doj = new DateTime (2014,01,23) , Sal =62500.50, Designation ="Team Lead" },
                new Employee{ DeptID=10, Name ="Swayam", Doj = new DateTime (2012,09,13) , Sal =27500.50, Designation ="Testing Engineer" },
                new Employee{ DeptID=2, Name ="Zeeshan", Doj = new DateTime (2016,09,18) , Sal =22500.50, Designation ="Developer" }

            };

            Console.WriteLine("Employee with salary less than 70000: ");
            var sallessthanseventy = from employeeObj in people
                                     where employeeObj.Sal <= 70000
                                     select employeeObj;
            foreach (Employee e in sallessthanseventy)
            {
                Console.WriteLine(e.Name);
            }

            Console.WriteLine("==================================================");

            Console.WriteLine("Employee who are manager or analyst: ");
            var manageroranalyst = from employeeObj in people
                                   where employeeObj.Designation == "Manager"
                                   || employeeObj.Designation == "System Analyst"
                                   || employeeObj.Designation == "Quality Analyst"
                                   select employeeObj;
            foreach (Employee e in manageroranalyst)
            {
                Console.WriteLine(e.Name);
            }

            Console.WriteLine("==================================================");

            Console.WriteLine("Employee from department 10 with salary less than 20000: ");
            var empfromtensaltwenty = from employeeObj in people
                                      where employeeObj.DeptID == 10 && employeeObj.Sal <= 20000
                                      select employeeObj;
            foreach (Employee e in empfromtensaltwenty)
            {
                Console.WriteLine(e.Name);
            }

            Console.WriteLine("==================================================");

            //ERROR NOT WHOLE DETAILS
            Console.WriteLine("details of employees whose name starts with ‘V’: ");
            var employeewithnamefromv = from employeeObj in people
                                        where employeeObj.Name[0] == 'V'
                                        select employeeObj;

            foreach (Employee e in employeewithnamefromv)
            {
                Console.WriteLine(e.Name);
            }

            Console.WriteLine("==================================================");

            Console.WriteLine("Employee name which has ‘o’ as 2 letter: ");
            var empwithnamesecondlettero = from employeeObj in people
                                           where employeeObj.Name[2] == 'o'
                                           select employeeObj;
            foreach (Employee e in empwithnamesecondlettero)
            {
                Console.WriteLine(e.Name);
            }
            Console.WriteLine("==================================================");


            Console.WriteLine("==================================================");

          

            Console.WriteLine("Update the salary of employees by 1000 for those drawing less than 60000: ");

            Employee employeeObj1 = new Employee();

            var updatesalbythousand = from employeeObj in people
                                      where employeeObj.Sal <= 60000
                                      select employeeObj;
            foreach (Employee e in updatesalbythousand)
            {
                employeeObj1.Sal += 1000;
                Console.WriteLine("***!!Update the Salary!!***");
            }

            Console.WriteLine("==================================================");

            Console.WriteLine("t the average salaries of employees department wise");

            var que7 = from employeeObj in people
                       group employeeObj by employeeObj.DeptID into EmpGroup
                       select new
                       {
                           DeptId = EmpGroup.Key,
                           averageSalary = EmpGroup.Average(x => x.Sal),
                       };
            foreach(var a in que7)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine("==================================================");

            Console.WriteLine("the records of top 2 employees drawing the highest salaries");


            var que8 = people.OrderByDescending(data => data.Sal).Select(data1 => data1.Sal).Distinct().Take(2);
            foreach(var b in que8)
            {
                Console.WriteLine(b);
            }
            Console.WriteLine("==================================================");

            Console.WriteLine("the sum, min, max and average salary of employees");
            var que9= from employeeObj in people
                      group employeeObj by employeeObj.DeptID into EmpGroup
                      select new
                      {

                          DeptId = EmpGroup.Key,
                          averageSalary = EmpGroup.Average(x => x.Sal),
                            minsal=EmpGroup.Min(x=>x.Sal),
                            maxsal=EmpGroup.Max(x=>x.Sal)
                      };
            foreach (var a in que9)
            {
                Console.WriteLine(a);
            }

            Console.ReadKey();
        }
       
    }
    }

